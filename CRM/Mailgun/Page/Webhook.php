<?php

use Civi\Mailgun\MailgunEvent;
use CRM_Mailgun_ExtensionUtil as E;

class CRM_Mailgun_Page_Webhook extends CRM_Core_Page {

  public function run() {
    $body = file_get_contents('php://input');
    $data = json_decode($body, TRUE);

    $signing_key = \Civi::settings()->get('mailgun_webhook_signing_key');
    if (!empty($signing_key)) {
      $signature = $data['signature'];
      /** @var \Civi\Mailgun\SignatureVerifier $signature_verifier */
      $signature_verifier = \Civi::service('mailgun.signature_verifier');

      if (!$signature_verifier->verify($signing_key, $signature['token'], $signature['timestamp'], $signature['signature'])) {
        CRM_Core_Error::debug('Mailgun_Webhook_Signature_Check_Failed', [
          'token' => $signature['token'],
          'signature' => $signature['signature'],
          'timestamp' => $signature['timestamp'],
          'body' => $body,
        ], TRUE, FALSE);

        http_response_code(400);
        CRM_Utils_System::civiExit();
      }
    }

    if (empty($data['event-data'])) {
        CRM_Core_Error::debug('Mailgun_Webhook_Bad_Payload', $data, TRUE, FALSE);

        http_response_code(400);
        CRM_Utils_System::civiExit();
    }

    $event = new MailgunEvent($data['event-data']['event'], $data['event-data']);
    \Civi::dispatcher()->dispatch(MailgunEvent::NAME, $event);

    CRM_Utils_System::civiExit();
  }

}
