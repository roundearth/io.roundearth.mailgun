<?php

namespace Civi\Mailgun;

/**
 * Service for verifying Mailgun signatures.
 */
class SignatureVerifier {

  /**
   * Verify a signature.
   *
   * @param string $signing_key
   *   The Mailgun Webhook signing key.
   * @param string $token
   *   A randomly generated string.
   * @param int $timestamp
   *   UNIX timestamp when the payload was sent.
   * @param string $signature
   *   The signature.
   *
   * @return bool
   */
  public function verify($signing_key, $token, $timestamp, $signature) {
    $hmac_digest = hash_hmac('sha256', $timestamp . $token, $signing_key);
    return hash_equals($signature, $hmac_digest);
  }

}