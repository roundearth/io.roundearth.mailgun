<?php

namespace Civi\Mailgun;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class Services {

  public static function registerServices(ContainerBuilder $container) {
    // Add our services.
    $container->setDefinition('mailgun.signature_verifier', new Definition(\Civi\Mailgun\SignatureVerifier::class));
    $container->setDefinition('mailgun.civimail_webhook_listener', new Definition(\Civi\Mailgun\CiviMailMailgunWebhookListener::class));

    // Add our event listeners.
    $container->findDefinition('dispatcher')
      ->addMethodCall('addListener', [\Civi\Mailgun\MailgunEvent::NAME,
        [new Reference('mailgun.civimail_webhook_listener'), 'processEvent']]);
  }

}
