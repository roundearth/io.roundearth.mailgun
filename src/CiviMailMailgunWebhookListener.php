<?php

namespace Civi\Mailgun;

/**
 * Listen for Mailgun webhooks and do stuff for CiviMail.
 */
class CiviMailMailgunWebhookListener {

  /**
   * Process a Mailgun event.
   *
   * @param \Civi\Mailgun\MailgunEvent $event
   */
  public function processEvent(MailgunEvent $event) {
    $payload = $event->getPayload();

    // Extract CiviMail info from the message sender.
    $civimail_info = NULL;
    if (!empty($payload['envelope']['sender'])) {
      $civimail_info = $this->parseCiviMailBounceAddress($payload['envelope']['sender']);
    }
    if (!$civimail_info) {
      // This doesn't appear to be a message from CiviMail. Skip it!
      return;
    }

    switch ($event->getName()) {
      case 'failed':
        \civicrm_api3('Mailing', 'event_bounce', [
          'job_id' => $civimail_info['job_id'],
          'event_queue_id' => $civimail_info['event_queue_id'],
          'hash' => $civimail_info['hash'],
          'body' => !empty($payload['delivery-status']['description']) ? $payload['delivery-status']['description'] : 'Bounced',
          // @todo Is it possible to reuse the same processing CiviMail does?
          // @todo Maybe check out 'BouncePattern'?
          'bounce_type_id' => !empty($payload['severity']) && $payload['severity'] == 'permanent' ? 6 : 11,
        ]);
        break;

      // @todo Re-enable once we're sure we won't double record opens from CiviMail
      //case 'opened':
      //  \CRM_Mailing_Event_BAO_Opened::open($civimail_info['event_queue_id']);
      //  break;

      case 'clicked':
        // @todo See https://github.com/imba-us/com.imba.sendgrid/blob/master/webhook.php#L142
        break;

    }
  }

  /**
   * Simplistic parsing of the X-CiviMail-Bounce header.
   *
   * @param string $address
   *   The bounce address.
   * @return array|null
   *   An array with the ids parsed out of the address or NULL if invalid.
   */
  protected function parseCiviMailBounceAddress($address) {
    list ($front,) = explode('@', $address);
    if (strpos($front, '+') !== FALSE) {
      list (, $front) = explode('+', $front);
    }
    $parts = explode('.', $front);
    if (count($parts) < 4) {
      return NULL;
    }

    return [
      'job_id' => $parts[1],
      'event_queue_id' => $parts[2],
      'hash' => $parts[3],
    ];
  }

}